import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Escolha a opção desejada:\n 1) Testa BancoVector\n 2) Testa BancoArray");
    int opcao = scanner.nextInt();
    switch(opcao) {
      case 1: testaBancoVector();
              break;
              
      case 2: testaBancoArray();
              break;
              
      default: System.out.println("Opção inexistente!");
              break;
    }
  }


  public static void testaBancoVector() {
    //escreva o código de teste aqui
    //lembre-se de testar o método renderJuros()
  }

  public static void testaBancoArray() {
    //escreva o código de teste aqui
    //lembre-se de testar o método renderJuros()
  }
  
}